import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { CarService } from '../services/car.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {
favoriteSeason: string;

  seasons = [
    'Winter',
    'Spring',
    'Summer',
    'Autumn',
  ];
  car = {};
  cars = [];
  isLoading = true;
  isEditing = false;

  addCarForm: FormGroup;
  brand = new FormControl('', Validators.required);
  model = new FormControl('', Validators.required);
  nDoors = new FormControl('', Validators.required);
  consumption = new FormControl('', Validators.required);
  airConditioner = new FormControl('', Validators.required);
  nPassengers = new FormControl('', Validators.required);

  constructor(private carService: CarService,
              private formBuilder: FormBuilder,
              private http: Http,
              public toast: ToastComponent) { }
  ngOnInit() {
    this.getCars();
    this.addCarForm = this.formBuilder.group({
      brand: this.brand,
      model: this.model,
      nDoors: this.nDoors,
      consumption : this.consumption ,
      airConditioner : this.airConditioner,
      nPassengers : this.nPassengers

    });
  }

  getCars() {
    this.carService.getCars().subscribe(
      data => this.cars = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  addCar() {
    this.carService.addCar(this.addCarForm.value).subscribe(
      res => {
        const newCar = res.json();
        this.cars.push(newCar);
        this.addCarForm.reset();
        this.toast.setMessage('item added successfully.', 'success');
      },
      error => console.log(error)
    );
  }

  enableEditing(car) {
    this.isEditing = true;
    this.car = car;
  }

  cancelEditing() {
    this.isEditing = false;
    this.car = {};
    this.toast.setMessage('item editing cancelled.', 'warning');
    // reload the cats to reset the editing
    this.getCars();
  }

  editCar(car) {
    this.carService.editCar(car).subscribe(
      res => {
        this.isEditing = false;
        this.car = car;
        this.toast.setMessage('item edited successfully.', 'success');
      },
      error => console.log(error)
    );
  }

  deleteCar(car) {
    if (window.confirm('Are you sure you want to permanently delete this item?')) {
      this.carService.deleteCar(car).subscribe(
        res => {
          const pos = this.cars.map(elem => elem._id).indexOf(car._id);
          this.cars.splice(pos, 1);
          this.toast.setMessage('item deleted successfully.', 'success');
        },
        error => console.log(error)
      );
    }
  }

}
